tls-certs
======================

Generate and/or deploy TLS certificate

# Examples

## Example to generate a self-signed TLS certificate

```YAML
 - hosts: all
   roles:
     - tls-certs
```

